# python-alpine-pipenv

Docker container to deploy apps using `pipenv`.

## Introduction

### What is `pipenv`

Excerpting from its [Github webpage](https://github.com/pypa/pipenv):

> `pipenv` is "the officially recommended Python packaging tool […] that aims to bring the best of all packaging worlds […] to the Python world."

It manages virtual environments, and uses a `Pipfile` (and specifically `Pipfile.lock`) to manage dependencies in a way that produces deterministic builds.

### Why not use the official container image

This uses Alpine Linux as its base, to keep the image size smaller and less prone to security issues.

## Usage

To use this file for your Python project, create a Dockerfile and use this as its base.

Here's a skeleton:

```dockerfile
FROM registry.gitlab.com/mhobson/python-alpine-pipenv:latest

# Copy source to app directory (this must contain Pipfile.lock)
COPY . /app
# Run your code
CMD [ "python", "/app/app.py" ]
```

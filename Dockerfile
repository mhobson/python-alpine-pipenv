# Based on official pipenv Dockerfile:
# https://github.com/pypa/pipenv/blob/master/Dockerfile
FROM python:3.7-alpine

# Update the distro packages and install pipenv
RUN apk --no-cache upgrade \
 && apk add --no-cache tzdata \
 && pip install --no-cache-dir --upgrade pip \
 && pip install --no-cache-dir --upgrade pipenv \
 && rm -rf /var/cache/apk/* /var/lib/apk/*

# Install Application into container:
RUN set -ex \
  && mkdir /app

WORKDIR /app

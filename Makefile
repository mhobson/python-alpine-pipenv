IMAGE = registry.gitlab.com/mhobson/python-alpine-pipenv
TAG = $(shell git describe --always --dirty)
TAGS = $(shell git tag --list --points-at)

.PHONY: all build push tag_latest push_latest run

all: build tag_all push push_all

build:
	docker build --no-cache -t $(IMAGE):$(TAG) .

push:
	docker push $(IMAGE):$(TAG)

tag_all:
	for ATAG in $(TAGS); do \
		docker tag $(IMAGE):$(TAG) $(IMAGE):$$ATAG ; \
	done

push_all:
	for ATAG in $(TAG) $(TAGS); do \
		docker push $(IMAGE):$$ATAG ; \
	done

clean:
	docker rmi $(IMAGE):$(TAG) &>/dev/null || true

run:
	docker run --rm -it $(IMAGE):$(TAG) sh
